/*
########## PIVOT SHIFT MANEUVER TELEMETRY ##########
  07/2014 by Jose Miguel Diaz
  jose.diaz.1988@ieee.org
  www.google.com/+JoseMiguelDiaz
based on work done by Adafruit on https://github.com/adafruit/Adafruit_LSM9DS0_Library

===============================================
Pivot shift maneuver telemetry is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License by Jose Miguel Diaz
http://creativecommons.org/licenses/by-sa/4.0/
===============================================

########## CHANGELOG ##########
28/09/2014  Added full support for 2 sensors
            Added RGB LED for status indication
27/08/2014  Modified the library for 2 sensors use
16/09/2014  Version 3.0, recode using the LSM9DS0 chip
06/08/2014  Modified the reset and noise detection functions
30/07/2014  Added the stub for detecting noise in acceleration
22/07/2014  Added the debugging flag for easier and cleaner debugging
            Added timeouts in several while() functions to eliminate some infinite loops
21/07/2014  Remade code from the I2Cdev documentation

*/

//########## DEBUGGING FLAG ##########
//Uncomment for debugging mode through the serial terminal:
//#define debuggingMode

//########## LIBRARIES ##########

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM9DS0.h>

//########## VARIABLES ##########

Adafruit_LSM9DS0 lsm1 = Adafruit_LSM9DS0(1000, 1); //First 9-DOF(SDOG and SDOXM on default VCC)
Adafruit_LSM9DS0 lsm2 = Adafruit_LSM9DS0(1000, 0); //Second 9-DOF (SDOG and SDOXM pulled to ground)

#define notificationLed 13 //Transmission indication LED
bool blinkState = 0;  //notificationLed state
bool transmission = 0; //Controls if MPUs are to be read
bool opMode = 1; //Accelerometers operation mode ( 0 = 2acc ; 1 = 1acc)
bool systemsReady = 0; //0 = No, 1 = yes
float gravityComponent1[3], gravityComponent2[3]; //What is the component for gravity on each acc axis
bool calibrationNedded = 1; //Is calibration needed?
byte statusLed[3] = {5, 6, 9}; //RGB LED connected to PWM pins (common anode)
sensors_event_t accel1, mag1, gyro1, temp1, accel2, mag2, gyro2, temp2; //Raw data variables
float x, y, z, w, p, r; //Final data variables

//########## SETUP ##########
void setup()
{
  colorLed(255, 0, 0);
  Serial.begin(19200);
  userSetup();
}

void userSetup()
{
  pinMode(notificationLed, OUTPUT);
  #ifdef debuggingMode
    Serial.println("\t\tDebugging mode ON");
  #endif
  Serial.println(F("Select Operation Mode ('a' = 1 9DOFs, 'b' = 2 9DOFs)..."));
  char s = 0;
  //while (Serial.available() <= 0) {} //Wait for user input
  while (!(s == 'a' || s == 'b'))
  {
    if (Serial.available() > 0)
    {
      //Select operatiion mode
      //Serial.println(F("Select Operation Mode ('a' = 1 9DOFs, 'b' = 2 9DOFs)..."));
      s = Serial.read();
      //Serial.print(F("receiving \t \t")); Serial.println(s);
      if (s == 'a') opMode = 1; //Select ONE 9-DOF mode
      else if (s == 'b') opMode = 0; //Select TWO 9-DOF mode
      //default mode is ONE 9-DOF
    }
  }

  while (!systemsReady) mpuSetup(); //Repeat setup until all systems are ready
}

//########## 9DOFs SETUP ##########
void mpuSetup()
{
  colorLed(50, 100, 0);

  /* Initialise the sensor */
  #ifdef debuggingMode
    Serial.print(F("\t\tTesting sensor 1..."));
  #endif
  bool lsm1Ready = lsm1.begin();
  if (lsm1Ready)
  {
    #ifdef debuggingMode
      Serial.println(F("\tSensor 1 OK"));
    #endif
  }
  bool lsm2Ready;
  if (!opMode)
  {
    #ifdef debuggingMode
      Serial.println(F("\t\tTesting sensor 2..."));
    #endif
    lsm2Ready = lsm2.begin();
    if (lsm2Ready)
    {
      #ifdef debuggingMode
        Serial.println(F("\tSensor 2 OK"));
      #endif
    }
  }
  if(!lsm1Ready && !lsm2Ready)
  {
    /* There was a problem detecting the LSM9DS0 ... check your connections */
    Serial.print(F("Ooops, no LSM9DS0 detected..."));
    colorLed(255, 0, 0);
    return;
  }
  if (lsm1Ready && !lsm2Ready)
  {
    Serial.println(F("\t1 9DOF detected"));
    opMode = 1;
  }
  if (lsm1Ready && lsm2Ready)
  {
    Serial.println(F("\t2 9DOFs detected"));
  }
  Serial.println(F("Found LSM9DS0 9DOF"));
  configureSensor();

  if (opMode) Serial.println(F("\tUsing 1 9DOF"));
  else Serial.println(F("\tUsing 2 9DOFs"));
  systemsReady = 1; //Let the setup know everything is ready
  Serial.println(F("Ready, Send 1 to begin transmission...")); //Indicate user everything is ready:
  colorLed(50, 100, 0);

}

//########## CONFIGURE SENSORS ##########
void configureSensor()
{
  colorLed(0, 0, 255);
  // 1.) Set the accelerometer range
  lsm1.setupAccel(lsm1.LSM9DS0_ACCELRANGE_2G);
  // 2.) Set the magnetometer sensitivity
  lsm1.setupMag(lsm1.LSM9DS0_MAGGAIN_2GAUSS);
  // 3.) Setup the gyroscope
  lsm1.setupGyro(lsm1.LSM9DS0_GYROSCALE_245DPS);
  if (!opMode)
  {
    lsm2.setupAccel(lsm2.LSM9DS0_ACCELRANGE_2G);
    lsm2.setupMag(lsm2.LSM9DS0_MAGGAIN_2GAUSS);
    lsm2.setupGyro(lsm2.LSM9DS0_GYROSCALE_245DPS);
  }
}

//########## LOOP ##########
void loop()
{
  if (Serial.available() > 0) //Receive a character and decide to go or stop
  {
    char s = Serial.read();
    Serial.print(F("receiving \t\t ")); Serial.println(s);
    if (s == '1')
    {
      transmission = 1; //Start
      colorLed(0, 255, 0);
    }
    else if (s == '0')
    {
      transmission = 0; //Stop
      digitalWrite(notificationLed, LOW);
      //calibrationNedded = 1;
      colorLed(50, 100, 0);
      systemsReady = 0;
      userSetup();
    }
  }
  if (transmission) //If told to start
  {
    //if (calibrationNedded) calibrate();
    if (opMode)
    {
      getData1();
      x = accel1.acceleration.x;// - gravityComponent1[0];
      y = accel1.acceleration.y;// - gravityComponent1[1];
      z = accel1.acceleration.z;// - gravityComponent1[2];
      w = gyro1.gyro.x;
      p = gyro1.gyro.y;
      r = gyro1.gyro.z;
      printData(x, y, z, w, p, r);
    }
    else
    {
      getData1();
      getData2();
      //x = accel1.acceleration.x - gravityComponent1[0] - accel2.acceleration.x + gravityComponent2[0];
      //y = accel1.acceleration.y - gravityComponent1[1] - accel2.acceleration.y + gravityComponent2[1];
      //z = accel1.acceleration.z - gravityComponent1[2] - accel2.acceleration.z + gravityComponent2[2];
      x = accel1.acceleration.x - accel2.acceleration.x;
      y = accel1.acceleration.y - accel2.acceleration.y;
      z = accel1.acceleration.z - accel2.acceleration.z;
      w = gyro1.gyro.x - gyro2.gyro.x;
      p = gyro1.gyro.y - gyro2.gyro.y;
      r = gyro1.gyro.z - gyro2.gyro.z;
      printData(x, y, z, w, p, r);
    }
    blinkState = !blinkState;
    digitalWrite(notificationLed, blinkState);
  }
  calibrationNedded = 0;
}

//########## GET DATA FROM SENSOR 1 ##########
void getData1()
{
  lsm1.getEvent(&accel1, &mag1, &gyro1, &temp1);
}

//########## GET DATA FROM SENSOR 2 ##########
void getData2()
{
  lsm2.getEvent(&accel2, &mag2, &gyro2, &temp2);
}

//########## PRINT DATA ##########
void printData(float x, float y, float z, float w, float p, float r)
{
  colorLed(0, 255, 0);
  #ifdef debuggingMode
    Serial.println("\t\tPrinting data");
  #endif
  //Print all the data from the accelerometers
  Serial.flush(); //flush serial buffer
  Serial.print("x "); Serial.println(x);
  Serial.print("\ty "); Serial.println(y);
  Serial.print("\t\tz "); Serial.println(z);
  Serial.flush(); //flush serial buffer
  Serial.print("\t\t\tw "); Serial.println(w);
  Serial.print("\t\t\t\tp "); Serial.println(p);
  Serial.print("\t\t\t\t\tr "); Serial.println(r);
  Serial.flush(); //flush serial buffer
}

//########## CALIBRATO FOR GRAVITY ##########
void calibrate()
{
  colorLed(0, 0, 255);
  Serial.println("Calibrating...");
  float averageX1, averageY1, averageZ1, averageX2, averageY2, averageZ2;

  for (byte i = 0; i < 10; i++) //Get an average acceleration measurement
  {
    getData1();
    if (i > 0)
    {
      averageX1 += accel1.acceleration.x;
      averageX1 /= 2;
      averageY1 += accel1.acceleration.y;
      averageY1 /= 2;
      averageZ1 += accel1.acceleration.z;
      averageZ1 /= 2;
      if (!opMode)
      {
        getData2();
        if (i > 0)
        {
          averageX2 += accel2.acceleration.x;
          averageX2 /= 2;
          averageY2 += accel2.acceleration.y;
          averageY2 /= 2;
          averageZ2 += accel2.acceleration.z;
          averageZ2 /= 2;
        }
      }
      else //For the first measurement:
      {
        averageX1 = accel1.acceleration.x;
        averageY1 = accel1.acceleration.y;
        averageZ1 = accel1.acceleration.z;
        if (!opMode)
        {
          averageX2 = accel2.acceleration.x;
          averageY2 = accel2.acceleration.y;
          averageZ2 = accel2.acceleration.z;
        }
      }
    }
    gravityComponent1[0] = averageX1;
    gravityComponent1[1] = averageY1;
    gravityComponent1[2] = averageZ1;
    if (!opMode)
    {
      gravityComponent2[0] = averageX2;
      gravityComponent2[1] = averageY2;
      gravityComponent2[2] = averageZ2;
    }
    Serial.println("Calibration done");
  }
}

//########## SET STATUS LED COLOR ##########
void colorLed(byte red, byte green, byte blue)
{
  //The RGB LED is common anode
  analogWrite(statusLed[0], 255 - red);
  analogWrite(statusLed[1], 255 - green);
  analogWrite(statusLed[2], 255 - blue);
}
