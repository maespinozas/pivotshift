


//Plot array
private void Plotarray(ArrayList array1, ArrayList array2, String strokecolor) //time,axis(x,y,z)
{
  if (strokecolor=="R")
  {
    stroke(230, 10, 10);
  }
  else if (strokecolor=="G")
  {
    stroke(10, 230, 10);
  }
  else if (strokecolor=="B")
  {
    stroke(10, 10, 230);
  }
  else if (strokecolor=="Y")
  {
    stroke(255, 232, 77);
  }

  strokeWeight(2);
  float xplot,x2plot,yplot,y2plot, upperLimit ; //remapped values
  upperLimit=array1.size()-1;    //arraytime , x axis array
  for(int i=0;i<upperLimit;i++)
  {
    if(i>0){
      xplot=map((Float)array1.get((i-1)),1,maxtime,plotX1, plotX2);
      x2plot=map((Float)array1.get(i),1,maxtime,plotX1, plotX2);
      yplot=map((Float)array2.get((i-1)),minvalue,maxvalue,plotY2, plotY1);//yplot=map((Float)array2.get((i-1)),0,1000,plotY2, plotY1);
      y2plot=map((Float)array2.get(i),minvalue,maxvalue,plotY2, plotY1);
      line(xplot,yplot,x2plot,y2plot);
      //also a point..
      strokeWeight(4);
      point(xplot,yplot);point(x2plot,y2plot);
      strokeWeight(2);
      }

  }

}

//Plot array
private void Plotarray_dual(ArrayList array1, ArrayList array2, String strokecolor, int position) //time,axis(x,y,z), position 0 only one graph,position 1 top, 2 bottom
{
  if (strokecolor=="R")
  {
    stroke(230, 10, 10);
  }
  else if (strokecolor=="G")
  {
    stroke(10, 230, 10);
  }
  else if (strokecolor=="B")
  {
    stroke(10, 10, 230);
  }
  else if (strokecolor=="Y")
  {
    stroke(255, 232, 77);
  }

  //stroke(230, 230, 230);
  //stroke(230, 230, 230,150);
  //stroke(0, 0, 0,150);
  //stroke(214, 133, 29);
  strokeWeight(2);
  float xplot,x2plot,yplot,y2plot, dif ; //remapped values
  if(array1.size()>array2.size())
  {
    //arraytime its bigger
    dif=array1.size()-array2.size();
  }
  else
  {
    dif=1;
  }
  float y1=0;
  float y2=0;
  if(position==1){ //upper graph
    y1=plotY1_1;
    y2=plotY2_1;
  }
  else if(position==2)  //lower graph
  {
    y1=plotY1_2;
    y2=plotY2_2;
  }
  else if(position==0)  //Default only acceleration graph
  {
    y1=plotY1;
    y2=plotY2;
  }

  for(int i=0;i<array1.size()-dif;i++)
  {
    if(i>0){
      xplot=map((Float)array1.get((i-1)),1,maxtime,plotX1, plotX2);
      x2plot=map((Float)array1.get(i),1,maxtime,plotX1, plotX2);
      yplot=map((Float)array2.get((i-1)),minvalue,maxvalue,y2, y1);//yplot=map((Float)array2.get((i-1)),0,1000,plotY2, plotY1);
      y2plot=map((Float)array2.get(i),minvalue,maxvalue,y2, y1);
      line(xplot,yplot,x2plot,y2plot);
      //plaintext.println((Float)array1.get((i))+","+(Float)array2.get((i)));

    }
  }

}

private void plotAgain(String mode) //clean the background before using this function
{
        if(mode=="acceleration"){
              maxtime=(getmaxvalue(arraytime));
              if (graphMode==true){ //only acceleration
                draw2(); //cleans plot area for 1 plot mode
                if(graphMag==true){                //XYZ
                maxvalue=max(getmaxvalue(arrayx),getmaxvalue(arrayy),getmaxvalue(arrayz));
                minvalue=min(getminvalue(arrayx),getminvalue(arrayy),getminvalue(arrayz));
                Plotarray(arraytime,arrayx,"R");
  
                Plotarray(arraytime,arrayy,"G");
  
                Plotarray(arraytime,arrayz,"B");}
                else{              
                  //Magnitude XYZ
                  maxvalue=getmaxvalue(arrayMagxyz);
                  minvalue=getminvalue(arrayMagxyz);
                  Plotarray(arraytime,arrayMagxyz,"Y");
                }
  
                textFont(displayFont);
                fill(200,0,0);
                text(("X:"+ (arrayx.get(arrayx.size()-1))),(plotX2-plotX1)/3,height-15);
                fill(200,200,200);
                text(("X:"+ (arrayx.get(arrayx.size()-1))),(plotX2-plotX1)/3,height-14);
                fill(0,200,0);
                text(("Y:"+ (arrayy.get(arrayy.size()-1))),(plotX2-plotX1)/2,height-15);
                fill(200,200,200);
                text(("Y:"+ (arrayy.get(arrayy.size()-1))),(plotX2-plotX1)/2,height-14);
                fill(0,0,200);
                text(("Z:"+ (arrayz.get(arrayz.size()-1))),(plotX2-plotX1)/1.5,height-15);
                fill(200,200,200);
                text(("Z:"+ (arrayz.get(arrayz.size()-1))),(plotX2-plotX1)/1.5,height-14);
                fill(277,255,77);
                text(("XYZ:"+ (arrayMagxyz.get(arrayMagxyz.size()-1))),(plotX2-plotX1)/1.2,height-14);
              }
              else //dual graphs , acceleration on top
              {
              cleanplotarea(1);
                if(graphMag==true){
                  maxvalue=max(getmaxvalue(arrayx),getmaxvalue(arrayy),getmaxvalue(arrayz));
                  minvalue=min(getminvalue(arrayx),getminvalue(arrayy),getminvalue(arrayz));
                  Plotarray_dual(arraytime,arrayx,"R",1);
    
                  Plotarray_dual(arraytime,arrayy,"G",1);
    
                  Plotarray_dual(arraytime,arrayz,"B",1);
                }
                else{                                      //Magnitude XYZ
                  maxvalue=getmaxvalue(arrayMagxyz);
                  minvalue=getminvalue(arrayMagxyz);
                  Plotarray_dual(arraytime,arrayMagxyz,"Y",1);
                }
              }
         }
         else{ //wpr              
              if(graphMag==true){
                maxtime=(getmaxvalue(arraytime));
                maxvalue=max(getmaxvalue(arrayw),getmaxvalue(arrayp),getmaxvalue(arrayr));
                minvalue=min(getmaxvalue(arrayx),getmaxvalue(arrayy),getminvalue(arrayz));
                minvalue=min(getminvalue(arrayw),getminvalue(arrayp),getminvalue(arrayr));
                cleanplotarea(2);
                Plotarray_dual(arraytime,arrayw,"R",2);
                Plotarray_dual(arraytime,arrayp,"G",2);
                Plotarray_dual(arraytime,arrayr,"B",2);  
              }
              else{
                maxtime=(getmaxvalue(arraytime));
                maxvalue=getmaxvalue(arrayMagwpr);
                minvalue=getminvalue(arrayMagwpr);
                cleanplotarea(2);
                Plotarray_dual(arraytime,arrayMagwpr,"Y",2);            
              }

              /*if (graphMode==true){ //only wpr
              Plotarray(arraytime,arrayx,"R");

              Plotarray(arraytime,arrayy,"G");

              Plotarray(arraytime,arrayz,"B");

              textFont(displayFont);
              fill(200,0,0);
              text(("W:"+ (arrayx.get(arrayw.size()-1))),(plotX2-plotX1)/3,height-15);
              fill(200,200,200);
              text(("W:"+ (arrayx.get(arrayw.size()-1))),(plotX2-plotX1)/3,height-14);
              fill(0,200,0);
              text(("P:"+ (arrayy.get(arrayp.size()-1))),(plotX2-plotX1)/2,height-15);
              fill(200,200,200);
              text(("P:"+ (arrayy.get(arrayp.size()-1))),(plotX2-plotX1)/2,height-14);
              fill(0,0,200);
              text(("R:"+ (arrayz.get(arrayr.size()-1))),(plotX2-plotX1)/1.5,height-15);
              fill(200,200,200);
              text(("R:"+ (arrayz.get(arrayr.size()-1))),(plotX2-plotX1)/1.5,height-14);

              }*/

         }
         if (graphMode==false){
           labels2graphs();}
              fill(valuesfontColor);
              textSize(10);
              text(numAccelerometers,width-200,height-10);
              //println(numAccelerometers);
              if(enableTestDuration==true){testduration(timelimit);}
              

}

private void plotAgainPosition() //arrayintx, arrayinty, arratintz
{
              if(arrayintx.size()>0 && arrayinty.size()>0 && arrayintz.size()>0)
              {

              maxtime=(getmaxvalue(arraytime));
              maxvalue=max(getmaxvalue(arrayintx),getmaxvalue(arrayinty),getmaxvalue(arrayintz));
              minvalue=min(getminvalue(arrayintx),getminvalue(arrayinty),getminvalue(arrayintz));
              textFont(titleFont);
              draw2();
              Plotarray(arraytime,arrayintx,"R");

              Plotarray(arraytime,arrayinty,"G");

              Plotarray(arraytime,arrayintz,"B");

              textFont(displayFont);
              fill(200,0,0);
              text(("X:"+ (arrayx.get(arrayintx.size()-1))),(plotX2-plotX1)/3,height-15);
              fill(200,200,200);
              text(("X:"+ (arrayx.get(arrayintx.size()-1))),(plotX2-plotX1)/3,height-14);
              fill(0,200,0);
              text(("Y:"+ (arrayy.get(arrayinty.size()-1))),(plotX2-plotX1)/2,height-15);
              fill(200,200,200);
              text(("Y:"+ (arrayy.get(arrayinty.size()-1))),(plotX2-plotX1)/2,height-14);
              fill(0,0,200);
              text(("Z:"+ (arrayz.get(arrayintz.size()-1))),(plotX2-plotX1)/1.5,height-15);
              fill(200,200,200);
              text(("Z:"+ (arrayz.get(arrayintz.size()-1))),(plotX2-plotX1)/1.5,height-14);
              labelx();
              }
              else
              {
                javax.swing.JOptionPane.showMessageDialog(null, "Error: please perform a test or use an .csv file");
              }
}

private void Cleargraph()
{
  arrayy.clear();arrayx.clear();arrayz.clear();
  arrayw.clear();arrayp.clear();arrayr.clear();
  arrayMagxyz.clear();arrayMagwpr.clear();
  arraytime.clear();
  sampleCount=0;
  filteredSamples=0;
  timeMin=0;
  maxvalue=0;
  minvalue=0;
  maxtime=0;
}

private void draw2()    //cleans blackground and paints plot rectangle (for 1 plot)
{
  textFont(titleFont);
  background(backgroundColor);  //clears background
  fill(0);
  text("Pivot Shift Test",(plotX2-plotX1)/2,80,-30); //300,50,-30
  fill(labelfontColor);
  text("Pivot Shift Test",(plotX2-plotX1)/2,81);
  strokeWeight(2);
  stroke(0);
  //plot area
  rectMode(CORNERS);
  fill(plotAreaColor);
  rect(plotX1, plotY1, plotX2, plotY2);
  fill(255,255,255);            //for white text
}




private ArrayList integrateArray2(ArrayList array)  //NOT ACCUMULATIVE INT VALUES
{
  ArrayList integratedArray= new ArrayList();
  float N=array.size()-1;//#number of trapezoids
  float ft1,ft2,prevIntVal,intVal;
  //println("Array size is:"+ N);
  //float a=0;
  //float b=(Float)(array.get(array.size()-1)); //last value
  for(int i=0;i<N;i++)
  {
    if(i==0)
    {
    //integratedArray.add(0.0);
    integratedArray.add((Float)(array.get(i))*.5);
    }
    else
    {
      //area=(t2-t1)*(f(t1)+f(t2)*0.5, t2-t1=1 equally spaced data points
      ft1=(Float)(array.get(i-1));
      ft2=(Float)(array.get(i));
      intVal=(ft1+ft2)*.5;
      //intVal=(ft1+ft2)*.5;
    integratedArray.add(intVal);//2f(xn)
    //println("Adding:"+intVal);
    }
  }

  return integratedArray;

}

private ArrayList integrateArray(ArrayList array)  //ACCUMULATING
{
  ArrayList integratedArray= new ArrayList();
  float N=array.size()-1;//#number of trapezoids
  float ft1,ft2,prevIntVal,intVal;
  //println("Array size is:"+ N);
  //float a=0;
  //float b=(Float)(array.get(array.size()-1)); //last value
  for(int i=0;i<N;i++)
  {
    //float x=0.5*(b/N);//(b-a)/2N
    if(i==0)
    {
    //integratedArray.add(0.0);
    integratedArray.add((Float)(array.get(i))*.5);
    }
    else
    {
      //area=(t2-t1)*(f(t1)+f(t2)*0.5, t2-t1=1 equally spaced data points
      ft1=(Float)(array.get(i-1));
      ft2=(Float)(array.get(i));
      prevIntVal=(Float)integratedArray.get(i-1);
      intVal=prevIntVal+(ft1+ft2)*.5;
      //intVal=(ft1+ft2)*.5;
    integratedArray.add(intVal);//2f(xn)
    //println("Adding:"+intVal);
    }
  }

  return integratedArray;

}

// ########## PLAIN TEXT FILE #################
private void writePlainTextWithIntegral()
{
println("---WritePlainText function---");
println("Arraytime size is:"+arraytime.size());
println("Arrayx size is:"+arrayx.size());
println("Arrayy size is:"+arrayy.size());
println("Arrayz size is:"+arrayz.size());
println("Array Int x size is:"+arrayintx.size());
println("Array Int y size is:"+arrayinty.size());
println("Array Int z size is:"+arrayintz.size());
int arraysize=min(arrayintx.size(),arrayinty.size(),arrayintz.size());
int arraywprSize=min(arrayw.size(),arrayp.size(),arrayr.size());
println("Arraysize:"+arraysize);
//header
plaintext.println("Eje x"+","+"Eje Y"+","+"Eje Z"+","+"Eje W"+","+"Eje P"+","
      +"Eje R"+","+"Integral X"+","+"Integral Y"+","+"Integral Z"+", MagXYZ , MagWPR");

if(arraywprSize==0)//In case a previous test was open without axis wpr information and wanted to calculate and save the integrals
{
    for(int i=0;i<arraysize;i++)
    {
      plaintext.println((Float)arrayx.get((i))+","+(Float)arrayy.get((i))+","+(Float)arrayz.get((i))
      +","+0+","+0+","+0+","+(Float)arrayintx.get(i)+","+(Float)arrayinty.get(i)+","+(Float)arrayintz.get(i)+","+(Float)arrayMagxyz.get(i)+", 0");
    }
}
else
{
    for(int i=0;i<arraysize;i++)
    {
      //print(i);
      plaintext.println((Float)arrayx.get((i))+","+(Float)arrayy.get((i))+","+(Float)arrayz.get((i))
      +","+(Float)arrayw.get((i))+","+(Float)arrayp.get((i))+","+(Float)arrayr.get((i))+","+(Float)arrayintx.get(i)+
      ","+(Float)arrayinty.get(i)+","+(Float)arrayintz.get(i)+","+(Float)arrayMagxyz.get(i)+","+(Float)arrayMagwpr.get(i));
    }
}
  plaintext.flush();
  plaintext.close();
  println("File saved");
}
// ########## PLAIN TEXT FILE #################
private void writeplaintext()
{
 println("arrayw size:"+ str(arrayw.size()));
 println("arrayp size:"+ str(arrayp.size()));
 println("arrayr size:"+ str(arrayr.size()));
int arrayswprData=min(arrayw.size(),arrayp.size(),arrayr.size());
int arraysize=min(arraytime.size(),arrayswprData);
println("arraysize is:"+ str(arraysize));
  for(int i=0;i<arraysize;i++)
  {
    plaintext.println((Float)arrayx.get((i))+","+(Float)arrayy.get((i))+","+(Float)arrayz.get((i))
    +","+(Float)arrayw.get((i))+","+(Float)arrayp.get((i))+","+(Float)arrayr.get((i)));
        //plaintext.println((Float)arrayx.get((i))+","+(Float)arrayy.get((i))+","+(Float)arrayz.get((i)));
  }
  plaintext.flush();
  plaintext.close();

}
//Plots graph using results from previous test
void openPrevGraph()
{
  JFileChooser chooser = new JFileChooser();

      chooser.setFileFilter(chooser.getAcceptAllFileFilter());
      int returnVal = chooser.showOpenDialog(null);
      if (returnVal == JFileChooser.APPROVE_OPTION)
    {
         String filename = chooser.getSelectedFile().getAbsolutePath();
         println("Loading File.."+ filename);
         if(filename.indexOf("csv")<=0)
         {
           javax.swing.JOptionPane.showMessageDialog(null, "Error: please use an .csv file");
         }
         else
         {
         controlP5.controller("Save Image").setVisible(true);
         String[] lines=loadStrings(filename);
         String[] stringfromfile;
         float contTime=1;//avoid x axis offset
         arrayx.clear();arrayy.clear();arrayz.clear();
         arrayw.clear();arrayp.clear();arrayr.clear();
         arrayintx.clear();arrayinty.clear();arrayintz.clear();
         arrayMagxyz.clear();arrayMagwpr.clear();
         arraytime.clear();
        //add data into arrays
        for(int i=1;i<lines.length;i++){
          stringfromfile=split(lines[i],",");   //to see if there is  wpr arrays in file
          if (stringfromfile.length<3){break;}
          println(stringfromfile);
          arrayx.add(float(stringfromfile[0]));
          arrayy.add(float(stringfromfile[1]));
          arrayz.add(float(stringfromfile[2]));
          arraytime.add(contTime);
          //check if arrays wpr exist in file
          if(stringfromfile.length>4)
          {
            arrayw.add(float(stringfromfile[3]));
            arrayp.add(float(stringfromfile[4]));
            arrayr.add(float(stringfromfile[5]));
            arrayintx.add(float(stringfromfile[6]));
            arrayinty.add(float(stringfromfile[7]));
            arrayintz.add(float(stringfromfile[8]));
          }
            //getting velocity
            arrayintx=integrateArray(arrayx);
            arrayinty=integrateArray(arrayy);
            arrayintz=integrateArray(arrayz);
            //Integrate x y and z arrays
            //getting position
            arrayintx=integrateArray(arrayintx);
            arrayinty=integrateArray(arrayinty);
            arrayintz=integrateArray(arrayintz);
            contTime+=1;
          }
          //get maximum and minimum values use in Plotarray function
          maxtime=(getmaxvalue(arraytime));
          maxvalue=max(getmaxvalue(arrayx),getmaxvalue(arrayy),getmaxvalue(arrayz));
          minvalue=min(getminvalue(arrayx),getminvalue(arrayy),getminvalue(arrayz));
          textFont(titleFont);
          draw2();
          Plotarray(arraytime,arrayx,"R");
          Plotarray(arraytime,arrayy,"G");
          Plotarray(arraytime,arrayz,"B");
          labelx();
          
          //check for magnitude arrays
          if(arrayMagxyz.size()==0 || arrayMagxyz.size()==0){
           println("Calculating magnitude arrays");
           arrayMagxyz=getMagVector(arrayx,arrayy,arrayz);
           arrayMagwpr=getMagVector(arrayw,arrayp,arrayr); 
          }
         }
    }
}

ArrayList getMagVector(ArrayList array1,ArrayList array2,ArrayList array3) //xyz or wpr arrays as input
{
  ArrayList magArray=new ArrayList();
  float m;
  for(int i=0;i<array1.size();i++){
    m=sqrt(pow((Float)array1.get(i),2)+pow((Float)array2.get(i),2)+pow((Float)array3.get(i),2));
    magArray.add(m);
  }
  return magArray;
}

void plotlayout2(){

//plot area rectangle
  textFont(titleFont);
  background(backgroundColor);
  fill(0);
  text("Pivot Shift Test",(plotX2-plotX1)/2,80,-30); //300,50,-30
  fill(200, 200, 200);
  text("Pivot Shift Test",(plotX2-plotX1)/2,81);
  fill(plotAreaColor);
  rectMode(CORNERS);
  //fill(100,40,40,40); //plotting area background
  //rect(plotX1, 100, plotX2, 400);
  rect(plotX1, plotY1_1, plotX2, plotY2_1);
  rect(plotX1, plotY1_2, plotX2, plotY2_2);
}

void cleanplotarea(int area){
//plot area rectangle
  fill(plotAreaColor);
  rectMode(CORNERS);
  //fill(100,40,40,40); //plotting area background
  //rect(plotX1, 100, plotX2, 400);
  stroke(0);
  if(area==1){ //upper graph
  rect(plotX1, plotY1_1, plotX2, plotY2_1);}
  else{
  rect(plotX1, plotY1_2, plotX2, plotY2_2);}

}

  private void labelx() //plot labels for acceleration mode
{
  stroke(255,255,255);
  fill(valuesfontColor);                //for text color
  float distx= (plotX2-plotX1)/10; //x axis increment
  float disty= (plotY2-plotY1)/10;  //y axis increment
  float incrementox=(Float)(arraytime.get(arraytime.size()-1))/10;
  float incrementoy=(maxvalue-minvalue)/10;
  for(int m=0;m<10;m++)             //10 equal spaced portions for the axis
  {
    textFont(labelFont);
    if(m==0)
    {
    text(minx,plotX1,plotY2+20);        //X axis values
    text(minvalue,plotX1-80,plotY2);    //Y axis values
    }
    else
    {
    text(minx+incrementox*(m),plotX1+(distx)*(m),plotY2+20);
    text(minvalue+incrementoy*(m),plotX1-80,plotY2-(disty)*(m));
    }
    if(m==5)
    {
    //text("dps",plotX1-15,round((plotY2+plotY1)*.5));
    //text("sample",round((plotX1+plotX2)*.5),plotY2+15);
    fill(labelfontColor);
    text("Samples",plotX1+(distx)*(m),plotY2+35);
    text("G's",plotX1-110,plotY2-(disty)*(m));
    fill(valuesfontColor);
    }
    //grid
    //stroke(40,80,150);
    //line(plotX1+(distx*m),plotY1,plotX1+(distx*m),plotY2);
    //line(plotX1,plotY1+(disty*m),plotX2,plotY1+(disty*m));
  }

}

private void labels2graphs() //plot labels for dual graphs
{
  fill(valuesfontColor); //Color blanco del texto
  float distx= (plotX2-plotX1)/10; //incremento  entre cada label
  float disty= (plotY2_1-plotY1_1)/10;
  float disty2=(plotY2_2-plotY1_2)/10;
  float maxacc=0.00; float minacc=0.00;
  float maxwpr=0.00; float minwpr=0.00;
  float incrementox=(Float)(arraytime.get(arraytime.size()-1))/10;
  if(graphMag==true){                          //XYZ 3 lines graph
  maxacc=max(getmaxvalue(arrayx),getmaxvalue(arrayy),getmaxvalue(arrayz));
  minacc=min(getminvalue(arrayx),getminvalue(arrayy),getminvalue(arrayz));
  maxwpr=max(getmaxvalue(arrayw),getmaxvalue(arrayp),getmaxvalue(arrayr));
  minwpr=min(getminvalue(arrayw),getminvalue(arrayp),getminvalue(arrayr));}
  else{                                        //magnitude of vector
  maxacc=getmaxvalue(arrayMagxyz);
  minacc=getminvalue(arrayMagxyz);
  maxwpr=getmaxvalue(arrayMagwpr);
  minwpr=getminvalue(arrayMagwpr);
  }
  float incrementoyacc=(maxacc-minacc)/10;
  float incrementoywpr=(maxwpr-minwpr)/10;
  for(int m=0;m<10;m++)
  {
    noStroke();
    rectMode(CORNER);
    fill(backgroundColor,15);
    rect(plotX1,plotY2_1+2, plotX2, 20); //Rectangle in labels for x
    rect(plotX1, plotY2_2+2,plotX2, 20); //Rectangle in labels for x
    rect(plotX1, plotY1_1+2,-plotX1,plotY2_2 ); //Rectangle covering y labels for both graphs
    fill(valuesfontColor); //Color blanco del texto
    stroke(255,255,255);

    textFont(labelFont);
    if(m==0)
    {
    text(minx,plotX1,plotY2_1+20);  //x val for 1st graph
    text(minacc,plotX1-80,plotY2_1);  //y val for 1st graph
    text(minx,plotX1,plotY2_2+20);
    text(minwpr,plotX1-80,plotY2_2);
    }
    else
    {
    text(minx+incrementox*(m),plotX1+(distx)*(m),plotY2_1+20);
    text(minacc+incrementoyacc*(m),plotX1-80,plotY2_1-(disty)*(m));
    text(minx+incrementox*(m),plotX1+(distx)*(m),plotY2_2+20);
    text(minwpr+incrementoywpr*(m),plotX1-80,plotY2_2-(disty2)*(m));
    }
    if(m==5)
    {
    fill(labelfontColor);
    //text("Samples",plotX1+(distx)*(m),plotY2+35);
    //text("G's",plotX1-110,plotY2-(disty)*(m));
    text("Samples",plotX1+(distx)*(m),plotY2_1+35);
    text("G's",plotX1-110,plotY2_1-(disty)*(m));
    text("Samples",plotX1+(distx)*(m),plotY2_2+35);
    //text("Gyroscope(Degrees)",plotX1-110,plotY2_2-(disty2)*(m));
      //for vertical text
      pushMatrix();
      translate(plotX1-100,plotY2_2-(disty2)*(2));
      rotate(-HALF_PI);
      text("Gyroscope(Degrees)",0,0);
      popMatrix();
    fill(255);
    }

  }
  
  displayreadings(true);


}

void displayreadings(Boolean gyrodatatoo) //shows acceleration and gryoscope data labels (text)
{
  
  //xyx text labels
  fill(200,0,0);
  text(("X:"+ (arrayx.get(arrayx.size()-1))),(plotX2-plotX1)/3,height-30);
  fill(200,200,200);
  text(("X:"+ (arrayx.get(arrayx.size()-1))),(plotX2-plotX1)/3,height-29);
  fill(0,200,0);
  text(("Y:"+ (arrayy.get(arrayy.size()-1))),(plotX2-plotX1)/2,height-30);
  fill(200,200,200);
  text(("Y:"+ (arrayy.get(arrayy.size()-1))),(plotX2-plotX1)/2,height-29);
  fill(0,0,200);
  text(("Z:"+ (arrayz.get(arrayz.size()-1))),(plotX2-plotX1)/1.5,height-30);
  fill(200,200,200);
  text(("Z:"+ (arrayz.get(arrayz.size()-1))),(plotX2-plotX1)/1.5,height-29);
  fill(277,255,77);
  text(("XYZ:"+ (arrayMagxyz.get(arrayMagxyz.size()-1))),(plotX2-plotX1)/1.2,height-30);
  if(gyrodatatoo==true){
    //rectangle to clean...x,y,w,h
    fill(backgroundColor); stroke(0);
    rect((plotX2-plotX1)/3,height-20,width,20);
    fill(200,0,0);
    text("Gyro-x: "+ nfs((Float)arrayw.get(arrayw.size()-1),1,2),(plotX2-plotX1)/3,height-10);
    fill(200,200,200);
    text("Gyro-x:"+ nfs((Float)arrayw.get(arrayw.size()-1),1,2),(plotX2-plotX1)/3,height-9);
    fill(0,200,0);
    text("Gyro-y:"+ nfs((Float)arrayp.get(arrayp.size()-1),1,2),(plotX2-plotX1)/2,height-10);
    fill(200,200,200);
    text("Gyro-y:"+ nfs((Float)arrayp.get(arrayp.size()-1),1,2),(plotX2-plotX1)/2,height-9);
    fill(0,0,200);
    text("Gyro-z:"+ nfs((Float)arrayr.get(arrayr.size()-1),1,2),(plotX2-plotX1)/1.5,height-13);
    fill(200,200,200);
    text("Gyro-z:"+ nfs((Float)arrayr.get(arrayr.size()-1),1,2),(plotX2-plotX1)/1.5,height-12);
    fill(277,255,77);
    text(("G-XYZ:"+ (arrayMagwpr.get(arrayMagwpr.size()-1))),(plotX2-plotX1)/1.2,height-13);
  }
  
}

Boolean checkpeak(ArrayList array, String sdata)
{
  if(array.size()>20){
    float lastvalue=abs((Float)array.get(array.size()-1));
    float currentval=abs(float(sdata));
    float ratio;

      ratio=abs(lastvalue-currentval);

    if (ratio>1000){
      println("Peak detected");
      return true;
    }
    else
    {
      return false;
    }
  }
  return false;

}

//###########  Promediado ventana 7 #######################
  //se toma el valor de p, n1,n2 y n3 son las 3 muestras anteriores
  // n3 ,n4, n5 las siguientes, se susitutye el valor de p por el promedio de los 7 numeros

private ArrayList promediado(ArrayList array,int startingFrom)
{
  float p,n1,n2,n3,n4,n5,n6;
  for(int i=startingFrom+7;i<array.size();i++)
  {
    if(i>6) // i=7, empieza a modificar a partir de la muestra 4
    {
      p=(Float)array.get(i-3); //numero a filtrar
      //filtro media aritmetica
        n1= (Float)array.get(i-6);
        n2=(Float)array.get(i-5);
        n3=(Float)array.get(i-4);
        n4=(Float)array.get(i-2);
        n5=(Float)array.get(i-1);
        n6=(Float)array.get(i);

        //promedio
        p=(n1+n2+n3+p+n4+n5+n6)/7;
        array.set((i-3),p);
    }
  }
  return array;

}
private ArrayList promediado(ArrayList array)
{
  float p,n1,n2,n3,n4,n5,n6;
  for(int i=0;i<array.size();i++)
  {
    if(i>6) // i=7, empieza a modificar a partir de la muestra 4
    {
      p=(Float)array.get(i-3); //numero a filtrar
      //filtro media aritmetica
        n1= (Float)array.get(i-6);
        n2=(Float)array.get(i-5);
        n3=(Float)array.get(i-4);
        n4=(Float)array.get(i-2);
        n5=(Float)array.get(i-1);
        n6=(Float)array.get(i);

        //promedio
        p=(n1+n2+n3+p+n4+n5+n6)/7;
        array.set((i-3),p);
        //println("Filtrando muestras");
    }
  }
  return array;

}
private ArrayList medianFilter(ArrayList array,int startingFrom)
{
  float p;
  ArrayList arrayMedian=new ArrayList();ArrayList arrayMedianTemp=new ArrayList();ArrayList arrayResult=new ArrayList();
  int windowSize=3;       // NEEDS TO BE ODD NUMBER
  int sampleToFilter=windowSize/2;
  //make copy of original array
  for(int i=0;i<array.size();i++){
    arrayResult.add(array.get(i));
  }


  for(int i=startingFrom;i<array.size()-1;i++)
  {
    if(i>windowSize-1) // i=7, empieza a modificar a partir de la muestra 4
    {
        /*arrayMedian.clear();
        arrayMedianTemp.clear();*/
        arrayMedianTemp=new ArrayList();
        arrayMedian=new ArrayList();
        for(int count=0;count<windowSize;count++){
          arrayMedian.add((Float)array.get(i-count));   //take the samples (windowSize samples), takes windowSize number of samples
          if(count==windowSize/2){sampleToFilter=i-windowSize/2;}
        }
        /*println("arrayMedian unsorted");
        println(arrayMedian);*/

        //SORTING ASCENDING
        float minval=0;int minvalIndex=0;
        while(arrayMedianTemp.size()<windowSize){            //arrayMedianTemp is used to sort ascending the window samples
        for(int count=0;count<arrayMedian.size();count++){  //gets minimum value and adds it to arrayMedianTemp array
          if(count==0){
            minval=(Float)(arrayMedian.get(count));
            minvalIndex=count;}
          if(minval>(Float)arrayMedian.get(count)){    //detect new minimum value
            minval=(Float)arrayMedian.get(count);
            minvalIndex=count;}
        }
        arrayMedianTemp.add(minval);
        arrayMedian.remove(minvalIndex);}

        arrayMedian=arrayMedianTemp;           //SORTED ARRAY ASCENDING
        p=(Float)arrayMedian.get(arrayMedian.size()/2); //THE MEDIAN SAMPLE
        /*println("arrayMedian sorted");
        println(arrayMedian);
        println("median is "+ p);*/
        //arrayResult.set((i-2),p);
        arrayResult.set(sampleToFilter,p);
        //println("Filtering sample "+i);    //DANGER SI NO EXISTE ESTE PRINT NO SE EJECUTA ALGUNAS VECES....

    }
    //else{println("not filtering i="+i);}
  }
  //println("Filtered samples from  "+startingFrom+ " interval");
  return arrayResult;

}

private ArrayList medianFilter(ArrayList array)  //TO USE IF AFTER TEST AND FILTER ALL
{
  float p;
  ArrayList arrayMedian=new ArrayList();ArrayList arrayMedianTemp=new ArrayList();ArrayList arrayResult=new ArrayList();
  int windowSize=9;       // NEEDS TO BE ODD NUMBER
  int sampleToFilter=windowSize/2;
  //make copy of original array
//  for(int i=0;i<array.size();i++){
//    arrayResult.add(array.get(i));
//  }

  for(int i=0;i<array.size();i++)  //for each sample in the array
  {
    if(i>windowSize-1) // need to have at least WindowSize of samples to take the median...
    {
        /*arrayMedian.clear();
        arrayMedianTemp.clear();*/
        arrayMedianTemp=new ArrayList();
        arrayMedian=new ArrayList();
        for(int count=0;count<windowSize;count++){
          arrayMedian.add((Float)array.get(i-count));   //take the samples (windowSize samples)
        }
        /*println("arrayMedian unsorted");
        println(arrayMedian);*/

        //SORTING ASCENDING
        float minval=0;int minvalIndex=0;
        while(arrayMedianTemp.size()<windowSize){       //arrayMedianTemp holds windowsize samples , if wsize=7 holds 7 samples
        for(int count=0;count<arrayMedian.size();count++){    //checks all windowsamples and gets the min value
          if(count==0){
            minval=(Float)(arrayMedian.get(count));
            minvalIndex=count;}
          if(minval>(Float)arrayMedian.get(count)){
            minval=(Float)arrayMedian.get(count);      //update the minimum value
            minvalIndex=count;}                        //index of that value
        }
        arrayMedianTemp.add(minval);                    //add value to temp array and remove from original
        arrayMedian.remove(minvalIndex);}

        arrayMedian=arrayMedianTemp;                   //SORTED ARRAY ASCENDING
        p=(Float)arrayMedian.get((arrayMedian.size()/2)+1); //THE MEDIAN
        //println("window size:"+windowSize+" Index of median sample is:" +arrayMedian.size()/2);
        /*println("arrayMedian sorted");
        println(arrayMedian);
        println("median is "+ p);*/
        //arrayResult.set((i),p);
        arrayResult.add(p);
        //println("Filtering sample "+i);
    }
  }
  //entregar desde muestra windowSize...
//  for(int j=0;j<windowSize/2;j++){
//    //println("Removing " +(Float) arrayResult.get(0));
//    arrayResult.remove(0);               //delete first non filter samples..
//    //int index2Remove=arraytime.size()-1;  //Remove last time
//    //arraytime.remove(index2Remove);
//    }



  return arrayResult;

}

void plotMedianFilter()
{
        //if (FilterMode==true){
              arrayx=medianFilter(arrayx);
              arrayy=medianFilter(arrayy);
              arrayz=medianFilter(arrayz);
              arrayw=medianFilter(arrayw);
              arrayp=medianFilter(arrayp);
              arrayr=medianFilter(arrayr);
              arrayMagwpr=medianFilter(arrayMagwpr);
              arrayMagxyz=medianFilter(arrayMagxyz);
        //redo integral arrays
              //Integrate x y and z arrays
              arrayintx=integrateArray(arrayx);
              arrayinty=integrateArray(arrayy);
              arrayintz=integrateArray(arrayz);
              //Integrate again to get position

              arrayintx=integrateArray(arrayintx);
              arrayinty=integrateArray(arrayinty);
              arrayintz=integrateArray(arrayintz);
       //recalculate arraytime
        timeMin=min(arrayz.size(),arrayy.size(),arrayx.size());
        arraytime.clear();
        for(Float t=1.0;t<timeMin;t++)
        {
          arraytime.add(t);
        }

              //plot again.. after filter
              if(graphMode==false){
                 plotlayout2();
                 background(backgroundColor);
                 plotAgain("acceleration");
                 plotAgain("wpr");
               }
               else{
                 plotAgain("acceleration");
                 //draw2();
                 labelx();
               }
               println("plot median filter");
      //}
}

void plotPromediadoFilter()
{
        //if (FilterMode==true){
              arrayx=promediado(arrayx);
              arrayy=promediado(arrayy);
              arrayz=promediado(arrayz);
              arrayw=promediado(arrayw);
              arrayp=promediado(arrayp);
              arrayr=promediado(arrayr);
        //redo integral arrays
              //Integrate x y and z arrays
              arrayintx=integrateArray(arrayx);
              arrayinty=integrateArray(arrayy);
              arrayintz=integrateArray(arrayz);
              //Integrate again to get position

              arrayintx=integrateArray(arrayintx);
              arrayinty=integrateArray(arrayinty);
              arrayintz=integrateArray(arrayintz);
       //recalculate arraytime
        timeMin=min(arrayz.size(),arrayy.size(),arrayx.size());
        arraytime.clear();
        for(Float t=1.0;t<timeMin;t++)
        {
          arraytime.add(t);
        }

              //plot again.. after filter
              if(graphMode==false){
                 plotlayout2();
                 background(backgroundColor);
                 plotAgain("acceleration");
                 plotAgain("wpr");
               }
               else{
                 plotAgain("acceleration");
                 //draw2();
                 labelx();
               }
               println("plot median filter");
      //}
}


Boolean Initialize()
{
  if (arraytime.size()>30){
    for(int x=0;x<28;x++)
    {
      arrayx.remove(x);
      arrayy.remove(x);
      arrayz.remove(x);
      arrayw.remove(x);
      arrayp.remove(x);
      arrayr.remove(x);
      arraytime.remove(x);
    }
    return true;
  }
  else {return false;}
}

Boolean checkpeakv2(ArrayList array, String sdata, String mode)
{
    if(array.size()>30){
   //if(Initialize()==true){
    float lastvalue=abs((Float)array.get(array.size()-1));
    float currentval=abs(float(sdata));
    float ratioAcc;float ratioWPR;
    float accThreshold=100; float wprThreshold=60;
    ratioAcc=abs(lastvalue-currentval*gravityConstant);
    ratioWPR=abs(lastvalue-currentval*180.0/PI);
    if(mode=="acc"){          //ACC
      if (ratioAcc>accThreshold){
        println("Peak detected "+ ratioAcc+" >" +accThreshold);
        return true; }
      else
      {
        return false;
      }
    }
    else if(mode=="wpr"){       //WPR
      if (ratioWPR>wprThreshold){
        println("wpr Peak detected "+ ratioWPR+" >" +wprThreshold);
        return true; }
      else
      {
        return false;
      }
    }

    }
    return false;
  }

void peakfilter()
{
  ArrayList arrayx1= new ArrayList();
  ArrayList arrayy1= new ArrayList();
  ArrayList arrayz1= new ArrayList();
  ArrayList arrayw1= new ArrayList();
  ArrayList arrayp1= new ArrayList();
  ArrayList arrayr1= new ArrayList();
  int iterations= arraytime.size()-1;
  float prevx,prevy,prevz,prevw,prevp,prevr;
  float cx,cy,cz,cw,cp,cr;
  float threshold=10;
  if (iterations>0){
    for(int i=0;i<iterations;i++){
      if(i==0){
        prevx=(Float)arrayx.get(i); prevy=(Float)arrayy.get(i); prevz=(Float)arrayz.get(i);
        prevw=(Float)arrayw.get(i); prevp=(Float)arrayp.get(i); prevr=(Float)arrayr.get(i);
        arrayx1.add(prevx);arrayy1.add(prevy);arrayz1.add(prevz);
        arrayw1.add(prevw);arrayp1.add(prevp);arrayr1.add(prevr);
      }
      else
      {
        prevx=(Float)arrayx.get(i-1); prevy=(Float)arrayy.get(i-1); prevz=(Float)arrayz.get(i-1);
        prevw=(Float)arrayw.get(i-1); prevp=(Float)arrayp.get(i-1); prevr=(Float)arrayr.get(i-1);
        cx=(Float)arrayx.get(i); cy=(Float)arrayy.get(i); cz=(Float)arrayz.get(i);
        cw=(Float)arrayw.get(i); cp=(Float)arrayp.get(i); cr=(Float)arrayr.get(i);
        //check threshold
        if(abs(cx-prevx)<threshold && abs(cy-prevy)<threshold && abs(cz-prevz)<threshold){
        //&& abs(cw-prevw)<threshold*2 && abs(cp-prevp)<threshold*2 && abs(cr-prevr)<threshold*2){ //take in mind wpr peaks too..
          arrayx1.add(cx);arrayy1.add(cy);arrayz1.add(cz);arrayw1.add(cw);arrayp1.add(cp);arrayr1.add(cr);
        }
      }
  }
    arrayx.clear();arrayy.clear();arrayz.clear();arrayw.clear();arrayp.clear();arrayr.clear();
    arrayMagxyz.clear();arrayMagwpr.clear();
    arrayx=arrayx1; arrayy=arrayy1; arrayz=arrayz1;arrayw=arrayw1;arrayp=arrayp1;arrayr=arrayr1;
    arraytime.clear();
    for(int i=1;i<arrayx.size();i++)
      {
        arraytime.add(float(i));
      }
    if(graphMode==true){ //acceleration mode
         plotAgain("acceleration");
         labelx();}
    else //2 graphs
     {
       plotAgain("acceleration");
       plotAgain("wpr");}
  }
  else{println("Nothing to do");}
}

float fixDec(float n, int d) {
  return Float.parseFloat(String.format("%." + d + "f", n));
}

//###############copy file#########################
private void copyfile(String srFile, String dtFile)
{
  try{
      File f1 = new File(srFile);
      File f2 = new File(dtFile);
      InputStream in = new FileInputStream(f1);

      //For Append the file.
//      OutputStream out = new FileOutputStream(f2,true);

      //For Overwrite the file.
      OutputStream out = new FileOutputStream(f2);

      byte[] buf = new byte[1024];
      int len;
      while ((len = in.read(buf)) > 0){
        out.write(buf, 0, len);
      }
      in.close();
      out.close();
      System.out.println("File copied.");
    }
    catch(FileNotFoundException ex){
      System.out.println(ex.getMessage() + " in the specified directory.");
      System.exit(0);
    }
    catch(IOException e){
      System.out.println(e.getMessage());
    }
  }

  void populateSerialDd(DropdownList ddl){
    // a convenience function to customize a DropdownList
    ddl.setBackgroundColor(color(190));
    ddl.setItemHeight(15);
    ddl.setBarHeight(25);
    ddl.setWidth(130);
    ddl.captionLabel().set("COM Port");
    ddl.captionLabel().style().marginTop = 3;
    ddl.captionLabel().style().marginLeft = 3;
    ddl.valueLabel().style().marginTop = 3;
    ddl.toUpperCase(false);
    //reverse population
    for(int i=myPort.list().length-1;i>=0;i--)
    {
          ddl.addItem(myPort.list()[i],i);
          if(i==myPort.list().length-1){
          ddl.captionLabel().set(myPort.list()[i]);
          dropDownItemName=myPort.list()[i];
          }//defaulting to last serial port
    }


    ddl.setColorBackground(color(60));
    ddl.setColorActive(color(255, 128));
    //ddl.setIndex(myPort.list().length);
  }

void switchgraphMode(){
 int value=0; 
 if(graphMode==true){value=0;}else{value=1;}
  controlP5.controller("graphMode").setValue(value);
}
void switchgraphMag(){
 int value=0; 
 if(graphMag==true){value=0;}else{value=1;}
  controlP5.controller("graphMag").setValue(value);
}

void testduration(int secs) //call this function inside plotAgain after printing numberofaccelerometers
{
  if(start==true){
    timestop=millis();
    float diff=timestop-timestart;
    if(diff>secs*1000){  //stop the test
    controlP5.controller("start").setValue(0);}
    //fill(200,0,0);
    if(diff>.8*secs*1000){
      fill(200,0,0);
    }
  textFont(labelFont); textSize(25);
  text("Time:"+diff+"ms",width-500,50);
  }  
}

void countdown(int secs){    //countdown to initialize the test.. Call this inside the draw() main loop
  secs-=1;    //stop when showing zero is more user friendly
     float endcountdown;
     endcountdown=millis();
     if(endcountdown-timestart<secs*1000-1000){
       endcountdown=millis();
       //Message
       rectMode(CORNERS);
       background(backgroundColor);
       textFont(titleFont); textSize(50);
       fill(23,135,217);
       text("Initializing.."+ Float.toString(fixDec((secs-(endcountdown-timestart)*.001),2)),((plotX2-plotX1)/2.0),(height/2.0),-30);            
       }
     else{
       //reset timestart
       cooldown=false;
       timestart=millis();
       println("End of countdown");
       }          

            
          
}



/*
2/20/14 Integral fixed, default to a serial port (last in the ports list)
        Shorcut ENTER to start test
3/3/14 Dual graphs enabled, TAB shorcut to switch between graphs available
3/22/14 Autoselect serial port upon start, disables start option until "Select op mode" message is recieved, user needs to hardware reboot arduino
3/27/14 Forze stop enable, 1 accelerometer default mode
5/29/14 changed code to calculate m/s in processing and difference between 2 accelerometers as well (arduino no longer does this)
7/6/14  Shorcut 's' to save a graph, remove filter of values -10023<value<+10023 (no longer needed)
7/11/14 Fix bug of wpr arrays not cleaning after a test
7/31/14 Implemented median filter to eliminate the spikes...

*/
