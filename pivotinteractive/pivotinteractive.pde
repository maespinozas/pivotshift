/**
 * Pivot shift processing
 *
 * Marco Antonio Espinoza Sanchez
 * maespinozas0188@gmail.com
 */
import processing.serial.*;
import processing.opengl.*;
import controlP5.*;
import java.io.*;
//import fullscreen.*;
import javax.swing.JFrame;
import javax.swing.JFileChooser;//save dialog



//serial
Serial myPort;                // Create object from Serial classbuffer
String portName;
String numAccelerometers="";  //used to display number of accelerometers used
boolean first_time=true;
boolean opMode=false;         //true=2 accelerometers, false=1 accelerometer
boolean graphMode=true;       //acceleration only, false 2 plots acceleration and wpr
boolean graphMag=true;       //plot XYZ values or magnitude vector, TRUE plots XYZ
color backgroundColor=color(2,2,2); color plotAreaColor=color(240,240,240,50);
//color backgroundColor=color(100,100,100); color plotAreaColor=color(30,30,30,150);
color labelfontColor=color(146,246,255); color valuesfontColor=color(250,250,250
);
Boolean go=false;             //flag used to start the test

float sampleCount=0; float sample4Init=50;    //number of samples to used as initialization
int filteredSamples=0;
float maxvalue,minvalue, maxtime;
String sdata,sdata2[],sdataf;                        //serial data
String dropDownItemName;                      //dropdown used for serial

int tempwidth, tempheigth;
float timeCont, timeMin;                      //to keep track of samples recieved by serial port

ArrayList arrayx= new ArrayList();            //arrays for  x,y,z w,p,r axes
ArrayList arrayy= new ArrayList();
ArrayList arrayz= new ArrayList();
ArrayList arrayw= new ArrayList();
ArrayList arrayp= new ArrayList();
ArrayList arrayr= new ArrayList();
ArrayList arrayintx=new ArrayList();    //integration arrays
ArrayList arrayinty=new ArrayList();
ArrayList arrayintz=new ArrayList();
ArrayList arrayMagxyz=new ArrayList();  //Array calculating magnitude 
ArrayList arrayMagwpr=new ArrayList();
      
      
ArrayList arraytime= new ArrayList();         //time
float neww,newp,newr;                       //1 radian =57.2957795 degrees
float tw0,tw1,tp0,tp1,tr0,tr1;
float toDegree=57.2957795;

float plotX1, plotY1;
float plotX2, plotY2;
//For dual graph layout
//plotX1 and plotX2 still used for this
float plotY1_1;
float plotY2_1; //end of first plot area
float plotY1_2; //start of second area for plot
float plotY2_2; //end of second plot area
float minx,maxx,miny,maxy;  //used in labels2graphs() function
PFont plotFont,titleFont,labelFont,controlFont,displayFont;   //some fonts
PrintWriter plaintext;                                        //used to create csv file

ControlP5 controlP5;

boolean start ;                   //to get state of start button
int colorActive=color(10,200,10);
int colorInactive=color(200,10,10);

float gravityConstant = 0.002452501;
Boolean FilterMode=false;   //USE OR NOT USE FILTERS TO REMOVE SPIKES..

//Interactive variables
float xo;
float yo;
float zoom=1;
/*## To enable Countdown before the test and to limit each test to some time limit    ##*/
boolean enableTestDuration=true;
  float timestart,timestop;    //to limit the test to some amount of seconds... using testduration() function
  int timelimit=10;             //time in seconds

boolean enableCountdown=true;    //using countdown() function
  boolean cooldown=false;  //time to wait before initializing the test
  int countdowntime=5;    


// ################ SETUP ##############3
void setup()
{
  //String portName = myPort.list()[0];
  //println(portName);
  size(round(displayWidth*.85), round(displayHeight*.85));
  //interactive
  xo=0;
  yo=0;
  background(backgroundColor);
    tempwidth=width;
    tempheigth= height;
  strokeWeight(2);
  //graph
  //Single graph layout
  plotX1 = 120;
  plotX2 = width - 120;
  plotY1 = 100; //70
  plotY2 = height - 85;
  //Dual layout
  plotY1_1=plotY1;
  plotY2_1=plotY1+plotY2*.40; //end of first plot area
  plotY1_2=plotY2_1+50; //start of second area for plot
  plotY2_2=plotY1_2+plotY2*.40; //end of second plot area

  //FONTS
  plotFont = createFont("SansSerif", 18);
  labelFont = createFont("SansSerif", 15);
  titleFont=createFont("SansSerif",30);
  controlFont=createFont("SansSerif",16);
  displayFont=createFont("Courier",25);
  textFont(titleFont);
  textSize(32);
  fill(0);
  text("Pivot Shift Test",(plotX2-plotX1)/2,80,-30); //300,50,-30
  fill(200, 200, 200);
  text("Pivot Shift Test",(plotX2-plotX1)/2,81);

  //plot area rectangle
  rectMode(CORNERS);
  fill(100,40,40,40); //plotting area background
  rect(plotX1, plotY1, plotX2, plotY2);

  //########### GUI #######################################################
  controlP5= new ControlP5(this);
  controlP5.setControlFont(controlFont);
  //controlP5.addButton("Clear Graph",1,120,20,60,30);
  Button btnSaveImage= controlP5.addButton("Save Image",1,370,8,120,25); //parameters : name, value (float), x, y, width, height
  btnSaveImage.captionLabel().toUpperCase(false);
  Button btnOPG=controlP5.addButton("Open Previous Graph",1,500,8,200,25);
  btnOPG.captionLabel().toUpperCase(false);
  Button btnPP=controlP5.addButton("Plot Position",1,710,8,120,25);
  btnPP.captionLabel().toUpperCase(false);
  Button btnRS=controlP5.addButton("Remove spikes",1,850,8,150,25);
  btnRS.setVisible(false);                                            //hide this button
  btnRS.captionLabel().toUpperCase(false);
  controlP5.addToggle("start")
            .setPosition(40,50)
            .setSize(50,20)
            .setValue(false)
            .setMode(ControlP5.SWITCH);
            //setLock(controlP5.getController("start"),true); //remove to test with Esplora
  Toggle t_opMode=controlP5.addToggle("opMode")  //opMode true=2 accelerometers, false 1 accelerometers
            .setPosition(110,50)
            .setSize(50,20)
            .setValue(false)
            .setLabel("1 Acc")
            .setMode(ControlP5.SWITCH);
 Toggle t_graphMode= controlP5.addToggle("graphMode")
            .setPosition(t_opMode.getPosition().x+200,50)
            .setSize(50,20)
            .setValue(true)
            .setLabel("Acceleration")
            .setMode(ControlP5.SWITCH);
 Toggle t_graphMag= controlP5.addToggle("graphMag")
            .setPosition(t_opMode.getPosition().x+75,50)
            .setSize(50,20)
            .setValue(true)
            .setLabel("XYZ")
            .setMode(ControlP5.SWITCH);


  controlP5.setColorBackground(color(30,30,30));
  //controlP5.controller("Save Image").setVisible(false);      //button always visible
    // Other parameters..
    //controlP5.setColorValue(color(0,0,0));
    controlP5.setColorForeground(color(221,168,9));
    //controlP5.setColorLabel(color(0,0,0));  //Text color (CONTROLP5)
  println(dataPath(""));

  //Dropdown for serial port
  DropdownList d1;
  d1=controlP5.addDropdownList("SerialDropdown")
        .setPosition(150,35)
        ;
        populateSerialDd(d1);
  draw2();
        //autoinitialize serial
        first_time=false;
        println("Selected serial port "+ dropDownItemName);
        portName = dropDownItemName;
        
        try{
        myPort = new Serial(this, portName, 19200);
        myPort.bufferUntil('\n');
        println("Starting test");}
        catch(Exception e){
          println("No serial port available");
        }
        
 if(enableCountdown==true){
   cooldown=true;
 }   
 

}

/*
controlP5.setColorForeground(0xff000000);
  controlP5.setColorBackground(100);
  controlP5.setColorActive(0xff000000);
*/

//################# DRAW #######################
void draw()

{
  translate(xo,yo);
  scale(zoom);
      if (start==true){
        controlP5.controller("start").setColorActive(colorActive);
        if(myPort.available()>0)
        {
           if(enableCountdown==true && cooldown==true){
           countdown(countdowntime);
           }   
          if(arraytime.size()>1)      //if there are some samples to plot already
          {
            maxtime=(getmaxvalue(arraytime));
            if(graphMode==true){ //acceleration mode
            plotAgain("acceleration");
            labelx();
            }
            else                 //2 graphs  acc and rotation
            {
              background(backgroundColor);
              plotAgain("acceleration");
              plotAgain("wpr");
            }
          }
        } //if arraytime>0
      }   //if serialport available
      else
      {
        controlP5.controller("start").setColorActive(colorInactive);
      }
  }

  //################FUNCTIONS#########################
private float getmaxvalue(ArrayList array1)
{
  //println("Getting initial max value:", array1.get(0));
  float maxval=(Float)array1.get(0);
  for(int i=0;i<array1.size();i++)
  {
    if(maxval<(Float)array1.get(i))
    maxval=(Float)array1.get(i);
  }
  //println("Getting max value:", maxval);
  return maxval;

}

private float getminvalue(ArrayList array1)
{
  float minval=(Float)array1.get(0);
  for(int i=0;i<array1.size();i++)
  {
    if(minval>(Float)array1.get(i))
    minval=(Float)array1.get(i);
  }
  return minval;

}





//##### GUI ##################################

void controlEvent(ControlEvent theEvent) {
  // DropdownList is of type ControlGroup.
  // A controlEvent will be triggered from inside the ControlGroup class.
  // therefore you need to check the originator of the Event with
  // if (theEvent.isGroup())
  // to avoid an error message thrown by controlP5.

  if (theEvent.isGroup()) {
    // check if the Event was triggered from a ControlGroup
    println("event from group : "+theEvent.getGroup().getValue()+" from "+theEvent.getGroup());
    //dropDownItemName=theEvent.getName();
    dropDownItemName=theEvent.getGroup().captionLabel().getText();
  }
  else if (theEvent.isController()) {
    println("event from controller : "+theEvent.getController().getValue()+" from "+theEvent.getController()
    +" with label "+theEvent.controller().label());
  }
  if(theEvent.controller().name()=="start")//Start the test
  {
    if(start==true)
    {
    timestart=millis();
        if(first_time==true)
        {
        println("Selected serial port "+ dropDownItemName);
        portName = dropDownItemName;
        myPort = new Serial(this, portName, 19200);
        }

        //clear the arrays to start new test
        Cleargraph();
        first_time=false;
        myPort.bufferUntil('\n');
        //delay(2000); //wait for arduino
        if(opMode==true){
        myPort.write('b'); //2 accelerometers mode
        myPort.write('1');
        }
        else{
        myPort.write('a'); //1 accelerometer mode
        myPort.write('1');
        }
    }
    else if(myPort.available()>0)// and start==false
    {
      myPort.write('0');
      delay(20);
      myPort.write('0');
      delay(20);
      myPort.write('0');
      println("Ending test");
      if (FilterMode==true){
          //background(backgroundColor);
          plotMedianFilter();
      }
      
      if(enableCountdown==true){  //enable for next test
        cooldown=true;
       }  


      //Integrate x y and z arrays
      //getting velocity
      arrayintx=integrateArray(arrayx);
      arrayinty=integrateArray(arrayy);
      arrayintz=integrateArray(arrayz);
      //Integrate again to get position
      arrayintx=integrateArray(arrayintx);
      arrayinty=integrateArray(arrayinty);
      arrayintz=integrateArray(arrayintz);

    }

  }
  if(theEvent.controller().name()=="Save Image")
  {
    // Create a new file in the sketch directory
      plaintext = createWriter("out.csv");
      //plaintext.println("Eje x"+","+"Eje Y"+","+"Eje Z"+","+"Eje W"+","+"Eje P"+","+"Eje R");
      //writeplaintext(); with no integrals
      writePlainTextWithIntegral(); //with integrals
      save("out.png");
      println("image saved");
      JFileChooser chooser = new JFileChooser();
      chooser.setFileFilter(chooser.getAcceptAllFileFilter());
      int returnVal = chooser.showSaveDialog(null);
      if (returnVal == JFileChooser.APPROVE_OPTION)
    {
      //println("You chose to open this file: " + chooser.getSelectedFile().getName());
      //println(savepositions); //path del sketch, para copiar positions.txt a path deseada con funcion copyfile
          //String filename = chooser.getSelectedFile().getName();
        String filename = chooser.getSelectedFile().getAbsolutePath();
        String strNumAcc="";
        if(opMode==true){
          strNumAcc="2ac";
        }
        else{strNumAcc="1ac";}
        println("filename:"+filename);
        copyfile(sketchPath("")+"out.csv",filename+strNumAcc+".csv");
        
        //SAVE TWO IMAGES XYZ AND MAG
          if(graphMode==true){ //graphMode=true means plot acceleration
            //Change to show both acc and angulation
            switchgraphMode();
          }
          if(graphMag==true){      //SHOWING XYZ ???
              save(filename+"XYZ"+strNumAcc+".png");
              //NOW SWITCH TO MAG
              switchgraphMag();
              save(filename+"MAG"+strNumAcc+".png");
          }
          else{                    //From MAG to XYZ
            save(filename+"MAG"+strNumAcc+".png");
              //NOW SWITCH TO MAG
              switchgraphMag();
              save(filename+"XYZ"+strNumAcc+".png");          
          }                       
        println("Images saved");
        
    }

  }
  if(theEvent.controller().name()=="Open Previous Graph")
  {
    openPrevGraph();
  }
    if(theEvent.controller().name()=="Plot Position")
  {
    //
    plotAgainPosition();
  }
   if(theEvent.controller().name()=="Remove spikes")
  {
    plotMedianFilter();
    plotPromediadoFilter();
  }
   if(theEvent.controller().name()=="graphMode")
  {
    //println("Graphmode state:", graphMode);
    //
    if(graphMode==true){ //graphMode=true means plot acceleration
    controlP5.controller("graphMode").setLabel("Acceleration");
    draw2();
    controlP5.controller("Plot Position").setVisible(true);
    }
    else{
    controlP5.controller("graphMode").setLabel("Acc. and Position");
    plotlayout2();
    println("Plot mode 2");
    controlP5.controller("Plot Position").setVisible(false);
    }

    //to switch between graphs
    if(((arrayx.size()>1)) && ((arrayy.size()>1))&& ((arrayz.size()>1))&& arraytime.size()>1)
      {
         if(graphMode==false){
           background(backgroundColor);
           plotAgain("acceleration");
           plotAgain("wpr");
         }
         else{
           plotAgain("acceleration");
           //draw2();
           labelx();
         }
      }

  }
    if(theEvent.controller().name()=="opMode")
  {
      if(opMode==true){controlP5.controller("opMode").setLabel("2 Acc");}
      else {controlP5.controller("opMode").setLabel("1 Acc");}
      background(backgroundColor);
      if(graphMode==true){
        draw2();
      }
      else{plotlayout2();}

  }
  if(theEvent.controller().name()=="graphMag"){
    print(graphMag);
    if(graphMag==true){controlP5.controller("graphMag").setLabel("XYZ");}
    else{controlP5.controller("graphMag").setLabel("VMagnitude");}
    draw2();
    
    //to switch between graphs
    if(((arrayx.size()>1)) && ((arrayy.size()>1))&& ((arrayz.size()>1))&& arraytime.size()>1)
      {
         if(graphMode==false){
           background(backgroundColor);
           plotAgain("acceleration");
           plotAgain("wpr");
         }
         else{
           plotAgain("acceleration");
           //draw2();
           labelx();
         }
      }

  
  }

} //end control event


// ############ LOCK BUTTONS ######################
void setLock(Controller theController, boolean theValue) {
  theController.setLock(theValue);
  if(theValue==true) {
    //theController.setColorBackground(color(100,100));
    theController.setColorBackground(color(100,100));
  } else {
    theController.setColorBackground(color(100,100,100));
  }
}

   //##### SERIAL PORT EVENT #############
  void serialEvent(Serial myPort) {
  float value;
  //sdata = myPort.readStringUntil(13);//pivot shift
  sdata=(new String(myPort.readBytesUntil('\n')));//for prototype
  //println(sdata);
  if(sdata.indexOf("Using 2 Accelerometers")>=0){
    numAccelerometers="Using 2 Accelerometers";
  }
  else if(sdata.indexOf("Using 1 Accelerometer")>=0){
    numAccelerometers="Using 1 Accelerometer";
  }
  else if(sdata.indexOf("Select Operation Mode")>=0){
    setLock(controlP5.getController("start"),false);
    javax.swing.JOptionPane.showMessageDialog(null, "Ready to go, Press enter to start the test");
  }
  if(start==false && sdata.indexOf("x")>=0) //forzing stop
  {myPort.write('0');print("Forzing stop");}

  if(sdata.indexOf("Ready")>=0){
     go=true;             //to start the test
  }
  if(go && sdata.indexOf(' ')>=0){          //ex. x 300
  //println("sdata:"+sdata);
  value=float(split(sdata,' ')[1]);         //check the numerical value
  //sampleCount+=1;
  if(Float.isNaN(value)==false && cooldown==false){  //DANGER  if numerical value is good
      if(sdata.indexOf("x")>=0)//x axis
      {
        sdata2=split(sdata,' ');
        arrayx.add(float(sdata2[1]));
      }
      else if (sdata.indexOf("y")>=0)//y axis
      {
        sdata2=split(sdata,' ');
        arrayy.add(float(sdata2[1]));

      }
      else if (sdata.indexOf("z")>=0)//z axis
      {
        sdata2=split(sdata,' ');
        arrayz.add(float(sdata2[1]));
      }
      if (sdata.indexOf("w")>=0)//w axis
      {
        sdata2=split(sdata,' ');
        if(arrayw.size()==0){  //first element to initialize w with 0 degrees
          arrayw.add(0.00);
          tw0=millis();
        }
        else{
          tw1=millis();   //time new sample arrived
          println("(tw1-tw0)/1000.0","Angular-Vel","(tw1-tw0)/1000.0*Angular-Vel");
          println((Float)(tw1-tw0)/1000.0,float(sdata2[1]),(Float)(tw1-tw0)/1000.0*float(sdata2[1]));
          neww=((Float)arrayw.get(arrayw.size()-1))+((Float)(tw1-tw0)/1000.0)*float(sdata2[1]); //previous Rads+(tw1-tw0)*Rads/s
          //neww=((Float)(tw1-tw0)/1000.0)*float(sdata2[1]); //previous Rads+(tw1-tw0)*Rads/s
          arrayw.add(neww); //add new value to the last one
          tw0=tw1;  //for new incoming sample
          //adding raw data ..
          //arrayw.add(float(sdata2[1]));
        }
      }
      else if (sdata.indexOf("p")>=0)//p axis
      {
        sdata2=split(sdata,' ');
        if(arrayp.size()==0){  //first element to initialize w with 0 degrees
          arrayp.add(0.00);
          tp0=millis();
        }
        else{
          tp1=millis();   //time new sample arrived
          //println(tp1,tp0,tp1-tp0,(Float)(tp1-tp0)/1000.0);
          println("(tp1-tp0)/1000.0","Angular-Vel","(tp1-tp0)/1000.0*Angular-Vel");
          println((Float)(tp1-tp0)/1000.0,float(sdata2[1]),(Float)(tp1-tp0)/1000.0*float(sdata2[1]));
          newp=((Float)arrayp.get(arrayp.size()-1))+((Float)(tp1-tp0)/1000.0)*float(sdata2[1]); //previous Rads+(tw1-tw0)*Rads/s
          arrayp.add(newp); //add new value to the last one
          tp0=tp1;  //for new incoming sample
          //adding raw data ..
          //arrayp.add(float(sdata2[1]));
        }
      }
      else if (sdata.indexOf("r")>=0)//r axis
      {
        sdata2=split(sdata,' ');
        if(arrayr.size()==0){  //first element to initialize w with 0 degrees
          arrayr.add(0.00);
          tr0=millis();        
        }
        else{
          tr1=millis();   //time new sample arrived
          println("(tr1-tr0)/1000.0","Angular-Vel","(tr1-tr0)/1000.0*Angular-Vel");
          println((Float)(tr1-tr0)/1000.0,float(sdata2[1]),(Float)(tr1-tr0)/1000.0*float(sdata2[1]));
          newr=((Float)arrayr.get(arrayr.size()-1))+((Float)(tr1-tr0)/1000.0)*float(sdata2[1]); //previous Rads+(tw1-tw0)*Rads/s
          arrayr.add(newr); //add new value to the last one
          tr0=tr1;  //for new incoming sample
          //adding raw data ..
          //arrayr.add(float(sdata2[1]));
        }
      }

    timeMin=min(min(arrayz.size(),arrayy.size(),arrayx.size()),min(arrayw.size(),arrayp.size(),arrayr.size()));   //which has less samples
    //timeMin=min(arrayz.size(),arrayy.size(),arrayx.size());   //which has less samples
    //if(timeMin>min(arrayw.size(),arrayp.size(),arrayr.size())){timeMin=min(arrayw.size(),arrayp.size(),arrayr.size());}
    timeMin=timeMin+1;
    if ((arraytime.size()>0) && ((Float)arraytime.get(arraytime.size()-1)!=timeMin)) //last value.. condition to increment time array
     {
       arraytime.add(timeMin);
       //##update magnitude arrays##
       arrayMagxyz.add(sqrt(pow((Float)arrayx.get(arrayx.size()-1),2)+pow((Float)arrayy.get(arrayy.size()-1),2)+pow((Float)arrayz.get(arrayz.size()-1),2)));
       arrayMagwpr.add(sqrt(pow((Float)arrayw.get(arrayw.size()-1),2)+pow((Float)arrayp.get(arrayp.size()-1),2)+pow((Float)arrayr.get(arrayr.size()-1),2)));
     }
    else if(arraytime.size()==0) {    //add first element
    arraytime.add(timeMin);
    }
        //see number of samples for each axis
    /*
    println("x:" +arrayx.size()+" "+"y:" +arrayy.size()+" "+"z:" +arrayz.size()+" "+
          "gx:" +arrayw.size()+" "+"gy:" +arrayp.size()+" "+"gz:" +arrayr.size()+
          "  ##elapsed test time:"+(millis()-timestart)/1000.00); */

  }//filter values
  //else{println("Not accepted value:"+ str(value));}
  }//go end
  }

void stop ()
{
  if(myPort.available()>0)
  myPort.write('0');
  myPort.write('0');
  myPort.write('0');
  //myPort.write('0');
}


void keyPressed(){
  if (key==CODED){
   /* if(keyCode== UP){
      zoom += .03;
    } else if(keyCode ==DOWN){
      zoom -=.03;}*/
    }
    if (key == ' '){
      zoom=1;
      xo=0;
      yo=0;
    }
    if (key == ENTER || key==' '){
      int value=0;
      if(start==true){value=0;}else{value=1;}
      print("start="+start);
      controlP5.controller("start").setValue(value);
    }
    if (key == TAB ||key=='3'){
      int value=0;
      if(graphMode==true){value=0;}else{value=1;}
      controlP5.controller("graphMode").setValue(value);
    }
    if (key=='1'){
      int value=0;
      if(opMode==true){value=0;}else{value=1;}
      controlP5.controller("opMode").setValue(value);
    }
    if (key == '`'||key=='2'){
      int value=0;
      if(graphMag==true){value=0;}else{value=1;}
      controlP5.controller("graphMag").setValue(value);
    }
    if (key == 'S' || key=='s'){
      controlP5.controller("Save Image").setValue(1);
    }
       if (key == 'f' || key=='F'){
          plotMedianFilter();}
       if (key == 'p' || key=='P'){
          plotPromediadoFilter();}

}

void mouseDragged(){
  /*if (arraytime.size()>1){
  xo=xo+(mouseX-pmouseX);
  yo=yo+(mouseY-pmouseY);
  println("mouse dragged");
  }*/
}
